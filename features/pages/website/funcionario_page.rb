# Classe para métodos da funcionalidade referente a funcionário
# Essa classe extende do SitePrism::Page para poder mapear os elementos e 
# deixa-los mais facil para ser reutilizados
class FuncionarioPage < SitePrism::Page

  # Elementos mapeados
  # Usei o elements para mapear um elemento que retorna mais de um item
  # Usei o element para mapear um elemento unico
  elements :funcionario_novo,         'li.nav-item'
  element :funcionario_novo_nome,     '#inputNome'
  element :funcionario_novo_cpf,      '#cpf'
  element :funcionario_novo_sexo,     '#slctSexo'
  element :funcionario_novo_admissao, '#inputAdmissao'
  element :funcionario_novo_cargo,    '#inputCargo'
  element :funcionario_novo_salario,  '#dinheiro'
  element :funcionario_novo_tipo,     '#clt'
  element :funcionario_novo_btn,      '.cadastrar-form-btn'
  element :funcionario_novo_msg_ok,   '.alert'
  element :funcionario_pesquisar,     'input[aria-controls="tabela"]'
  element :funcionario_editar_btn,    '.btn-warning'
  element :funcionario_deletar_btn,   '.btn-danger'

  # Método para acessar a tela de cadastro de funcionário
  def acessar_tela_novo_funcionario
    funcionario_novo[1].click # O elements é utilizado passando o index do elemento desejado
  end

  # Método para preencher os dados de funcionário
  def preencher_funcionario(dados_funcionario)
    funcionario_novo_nome.set dados_funcionario[:func_nome]
    funcionario_novo_cpf.set dados_funcionario[:func_cpf]
    funcionario_novo_cpf.click
    funcionario_novo_sexo.select 'Indiferente'
    funcionario_novo_admissao.set dados_funcionario[:func_admissao]
    funcionario_novo_cargo.set dados_funcionario[:func_cargo]
    funcionario_novo_salario.set dados_funcionario[:func_salario]
    funcionario_novo_tipo.click
  end

  # Método para concluir o cadastro e a edição do funcionário
  def cad_edit_funcionario
    funcionario_novo_btn.click
  end

  # Método para pesquisar um funcionário
  def pesquisar_funcionario(funcionario_nome)
    funcionario_pesquisar.set funcionario_nome
  end

  # Método para acessar a edição do funcionário
  def acessar_tela_editar_funcionario
    # Como a tela não é atualizada após a pesquisa, usei o wait_until_<element>_visible
    # Para esperar pelo botão
    wait_until_funcionario_editar_btn_visible 
    funcionario_editar_btn.click
  end

  # Método para efetuar a exclusão do funcionário
  def deletar_funcionario
    # Como a tela não é atualizada após a pesquisa, usei o wait_until_<element>_visible
    # Para esperar pelo botão
    wait_until_funcionario_editar_btn_visible
    funcionario_deletar_btn.click
  end

  # Método para alterar somente o nome do funcionário
  def alterar_nome(funcionario_nome)
    funcionario_novo_nome.set funcionario_nome
  end
end
