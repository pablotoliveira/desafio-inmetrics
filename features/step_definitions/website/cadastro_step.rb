# Aqui é aberto a tela de cadastro de usuário por onde for passado no Caso de Teste
Given('acesso o cadastro de usuário pelo {string}') do |tp_link|
  cadastro.acessar_tela_cadastro(tp_link)
end

# Aqui é efetuado o cadastro do usuário desde o acesso a tela, 
# usando usuario e senha randômico pelo Faker
Given('cadastre um usuario pelo {string}') do |tp_link|
  @usuario_login = Faker::Name.name
  @senha_login = Faker::Base.numerify('Capybara####').to_s
  
  cadastro.acessar_tela_cadastro(tp_link)
  cadastro.preencher_cadastro(@usuario_login, @senha_login)
  cadastro.cadastrar
  login.wait_until_titulo_principal_visible
  expect(login.titulo_principal.text).to eql 'Login'
end

# Aqui é preenchido os dados do usuário, usando usuario e senha randômico pelo Faker
When('preencho os campos obrigatórios') do
  usuario_login = Faker::Name.name
  senha_login = Faker::Base.numerify('Capybara####').to_s
  cadastro.preencher_cadastro(usuario_login, senha_login)
end

# Aqui é efetuado o cadastro
When('clico em cadastrar') do
  cadastro.cadastrar
end

# Aqui é validado se o usuário foi cadastrado
Then('o usuario devera ser cadastrado') do
  expect(login.titulo_principal.text).to eql 'Login'
end