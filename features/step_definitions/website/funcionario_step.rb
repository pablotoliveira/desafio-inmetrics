# Aqui é aberto a tela de cadastro de funcionário
Given('acesso a tela de novo funcionario') do
  funcionario.acessar_tela_novo_funcionario
end

# Aqui é aberto a tela de edição de funcionário
Given('abro o funcionario {string} para edicao') do |funcionario_nome|
  funcionario.pesquisar_funcionario(funcionario_nome)
  funcionario.acessar_tela_editar_funcionario
end

# Aqui é efetuado a pesquisa do funcionário
Given('pesquiso o funcionario {string}') do |funcionario_nome|
  funcionario.pesquisar_funcionario(funcionario_nome)
end

# Aqui é efetuado o cadastro de um funcionário desde o acesso a tela, 
# usando dados randômicos pelo Faker, mas um nome passado pelo Caso de Teste
Given('tenha um funcionario cadastrado {string}') do |funcionario_nome|
  cpf = Faker::CPF.pretty
  admissao = Faker::Date.in_date_period(year: 2020, month: 8)
  cargo = Faker::Name.name
  salario = Faker::Number.number(digits: 6)

  dados_funcionario = Hash.new

  dados_funcionario = {
    :func_nome => funcionario_nome,
    :func_cpf => cpf,
    :func_admissao => admissao,
    :func_cargo => cargo,
    :func_salario => salario
  }

  funcionario.acessar_tela_novo_funcionario
  funcionario.preencher_funcionario(dados_funcionario)
  funcionario.cad_edit_funcionario
  expect(funcionario.funcionario_novo_msg_ok.text).to have_text 'SUCESSO! Usuário cadastrado com sucesso'
end

# Aqui é preenchido os dados do funcionário, usando dados randômicos pelo Faker
When('preencho todos os campos de funcionario') do
  @usuario = Faker::Name.name
  @cpf = Faker::CPF.pretty
  @admissao = Faker::Date.in_date_period(year: 2020, month: 8)
  @cargo = Faker::Name.name
  @salario = Faker::Number.number(digits: 6)

  dados_funcionario = Hash.new

  dados_funcionario = {
    :func_nome => @usuario,
    :func_cpf => @cpf,
    :func_admissao => @admissao,
    :func_cargo => @cargo,
    :func_salario => @salario
  }

  funcionario.preencher_funcionario(dados_funcionario)
end

# Aqui é efetuado o cadastro ou a edição do funcionário
When('clico em enviar') do
  funcionario.cad_edit_funcionario
end

# Aqui é deletado o funcionário
When('deleto o funcionario') do
  funcionario.deletar_funcionario
end

# Aqui é efetuado a alteração somente do nome do funcionário
When('altero o nome do funcionario para {string}') do |funcionario_nome|
  funcionario.pesquisar_funcionario(@usuario)
  funcionario.acessar_tela_editar_funcionario
  funcionario.alterar_nome(funcionario_nome)
  funcionario.cad_edit_funcionario
end

# Aqui é pesquisado o funcionário
When('pesquiso o funcionario editado {string}') do |funcionario_nome|
  funcionario.pesquisar_funcionario(funcionario_nome)
end

# Aqui é validado se o funcionário é cadastrado com sucesso
Then('o funcionario devera ser cadastrado com sucesso') do
  expect(funcionario.funcionario_novo_msg_ok.text).to have_text 'SUCESSO! Usuário cadastrado com sucesso'
end

# Aqui é validado se o funcionário é editado com sucesso
Then('o funcionario devera ser editado com sucesso') do
  expect(funcionario.funcionario_novo_msg_ok.text).to have_text 'SUCESSO! Informações atualizadas com sucesso'
end

# Aqui é validado se o funcionário é deletado com sucesso
Then('o funcionario devera ser deletado com sucesso') do
  expect(funcionario.funcionario_novo_msg_ok.text).to have_text 'SUCESSO! Funcionário removido com sucesso'
end