# Aqui é aberto o website com o link definido no env.rb
Given('que acesso o website') do
  visit '/'
end

# Aqui é feito o login desde o acesso ao website
Given('que eu esteja logado') do
  visit '/'
  login.preencher_login($data['usuario'],$data['senha'])
  login.fazer_login
end

# Aqui é preenchido os dados de login, vindo da variável global $data
# configurada no env.rb
When('preencho o usuario e a senha') do
  login.preencher_login($data['usuario'],$data['senha'])  
end

# Aqui é preenchido os dados de login, vindo do cadastro efetuado
When('preencho o usuario e a senha cadastrado') do
  login.preencher_login(@usuario_login, @senha_login)  
end

# Aqui é efetuado o login
When('clico em login') do
  login.fazer_login
end

# Aqui é validado se o usuário faz o login com sucesso
Then('o usuario devera ser logado com sucesso') do
  expect(login.navbar_links.map { |links| links.text }).to eq(['FUNCIONÁRIOS', 'NOVO FUNCIONÁRIO', 'SAIR'])
end
