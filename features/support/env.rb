# Inclusão das GEMs configuradas no projeto
require 'capybara'
require 'capybara/cucumber'
require 'site_prism'
require 'selenium-webdriver'
require "faker"
require "cpf_faker"
require 'httparty'
require 'httparty/request'
require 'httparty/response/headers'
require 'rspec'
require_relative 'page_helper.rb'

# Configurando as Pages como global
World(Pages)

# Configurando os dados nas variáveis
$data = YAML.load_file('./features/support/data/test_data.yaml')
$config = YAML.load_file('./features/support/config/config.yaml')

# Definindo as constantes a serem utilizadas no projeto
BROWSER          = ENV['BROWSER']               || 'chrome'
SCREENSHOT       = ENV['SCREENSHOT']            || 'all'
CAPYBARA_TIMEOUT = ENV['CAPYBARA_TIMEOUT']      || 10
BASE_URI         = $config['url_default']
USER_API         = $config['username']
PASS_API         = $config['password']
AUTH             = {username: USER_API, password: PASS_API}

# Configurando o driver do selenium como default a ser utilizado
# Configurando o link do website
# Configurando o timeout do projeto
Capybara.configure do |config|
  config.default_driver = :selenium_chrome
  config.app_host = 'http://www.inmrobo.tk/accounts/login'
  config.default_max_wait_time = CAPYBARA_TIMEOUT.to_i
end

# Configurando o driver do chrome
CAPS = Selenium::WebDriver::Remote::Capabilities.chrome(
  'chromeOptions' => {
    'args' => ['--window-size=1366,768',
               '--test-type',
               '--no-sandbox',
               '--incognito',
               '--no-cache'],
    'excludeSwitches' => ['--ignore-certificate-errors']
  }
)

# Configurando o driver do chrome para rodar em modo headless
CAPS_HEADLESS = Selenium::WebDriver::Remote::Capabilities.chrome(
  'chromeOptions' => {
    'args' => ['--window-size=1366,768',
               '--test-type',
               '--no-sandbox',
               '--incognito',
               '--headless',
               '--disable-gpu',
               '--disable-dev-shm-usage',
               '--no-cache'],
    'excludeSwitches' => ['--ignore-certificate-errors']
  }
)

# Método para rodar os testes no chrome
def register_chrome(app)
  Capybara::Selenium::Driver.new(app,
                                 browser: :chrome,
                                 desired_capabilities: CAPS)
end

# Método para rodar os testes no chrome em modo headless
def register_chromeheadless(app)
  Capybara::Selenium::Driver.new(app,
                                 browser: :chrome,
                                 desired_capabilities: CAPS_HEADLESS)
end

# Método para rodar os testes no modo definido na constante BROWSER
Capybara.register_driver :selenium do |app|
  send("register_#{BROWSER}", app)
end

# Método para efetuar as validações de retorno da API
RSpec::Matchers.define :match_response_schema do |schema|
  match do |response|
    JSON::Validator.validate!(schema, JSON.parse(response.body), strict: false)
  end
end
