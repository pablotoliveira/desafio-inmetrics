# Aqui eu incluo um require em todos os arquivos _page da pasta page
# para poder instancia-los
Dir[File.join(File.dirname(__FILE__),
    '../pages/*_page.rb')].each { |file| require file }

# Aqui é criado um módulo "Pages" para instanciar as classes
module Pages

  # Método para instanciar a classe de Cadastro
  def cadastro
    @cadastro ||= CadastroPage.new
  end

  # Método para instanciar a classe de Funcionário
  def funcionario
    @funcionario ||= FuncionarioPage.new
  end

  # Método para instanciar a classe de Login
  def login
    @login ||= LoginPage.new
  end

  #API
  # Método para instanciar a classe de Commons da API
  def commons_api
    @commons_api ||= CommonsApiPage.new
  end
end
